# 🍦Starter kit to quickly fire up vanilla HTML, CSS, JS projects

## 📦 Stuff included

-   Linters ([Stylelint](https://stylelint.io/), [Eslint](https://eslint.org/))
-   Precommit hooks for linting and image optimizing with [Husky](https://github.com/typicode/husky)
-   [Browserslist](https://browserl.ist/) to ensure consistent and transparent autoprefixing and linting
-   [Prettier](https://prettier.io/) for consistent code formatting
-   [Parcel](https://parceljs.org/) for bundling and development server
-   lazy.js in ./scripts folder to lazy load images using intersection-observer
    (To use lazy loading on any element, just apply the `class="lazy"` in your html.)

## 🏃‍♂️ Getting started

1. Clone the repo
1. Remove the `.git` folder (eg. running `rm -rf .git`)
1. Search for `!!!TEMPLATE!!!` across the project and replace these template names, texts and urls with your liking so you made the starter pack your own.
1. Create a new, empty README.md with eg. with running `rm README.md && touch README.md` in your terminal
1. Initialize a new git repository (eg. `git init` in your terminal)
1. Run `yarn` or `npm install` in your terminal to install dependencies
1. Run `yarn dev` or `npm run dev` to start the development server
1. Whenever you are ready for production, run`yarn build` to create a minified, optimized production-ready build
