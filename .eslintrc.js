module.exports = {
    parser: 'babel-eslint',
    extends: ['standard', 'plugin:prettier/recommended'],
    env: {
        browser: true,
        commonjs: true,
        es6: true,
        node: true,
    },
    plugins: ['compat', 'unicorn'],
    parserOptions: {
        ecmaFeatures: {
            jsx: true,
        },
        ecmaVersion: 2020,
        sourceType: 'module',
    },
    rules: {
        'compat/compat': 'error',
        'constructor-super': 'warn',
        'no-confusing-arrow': 'off',
        'no-const-assign': 'warn',
        'no-this-before-super': 'warn',
        'no-unreachable': 'warn',
        'valid-typeof': 'warn',
        indent: ['error', 4],
        quotes: ['error', 'single'],

        'unicorn/catch-error-name': ['off', { name: 'err' }],
        'unicorn/explicit-length-check': 'error',
        'unicorn/filename-case': ['off', { case: 'camelCase' }],
        'unicorn/no-abusive-eslint-disable': 'error',
        'unicorn/no-process-exit': 'error',
        'unicorn/throw-new-error': 'error',
        'unicorn/number-literal-case': 'error',
        'unicorn/escape-case': 'error',
        'unicorn/no-array-instanceof': 'error',
        'unicorn/no-new-buffer': 'error',
        'unicorn/no-hex-escape': 'error',
        'unicorn/custom-error-definition': 'off',
        'unicorn/prefer-starts-ends-with': 'error',
        'unicorn/prefer-type-error': 'error',
        'unicorn/no-fn-reference-in-iterator': 'off',
        'unicorn/import-index': 'error',
        'unicorn/new-for-builtins': 'error',
        'unicorn/regex-shorthand': 'error',
        'unicorn/prefer-spread': 'error',
        'unicorn/error-message': 'error',
        'unicorn/no-unsafe-regex': 'off',
        'unicorn/prefer-add-event-listener': 'error',
    },
};
